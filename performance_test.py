import pytest
from sqlalchemy import create_engine, text
import timeit
from dotenv import load_dotenv
import os

load_dotenv()

# URL для подключения
url = os.getenv("DB_URL")

# Создать объект engine
engine = create_engine(url)

@pytest.fixture
def test_db():
    # Возвращаем соединение к базе данных
    with engine.connect() as connection:
        yield connection

def run_query(test_db, query, pattern, table_name):
    # Измерение времени выполнения запроса
    start_time = timeit.default_timer()
    result = test_db.execute(query)
    end_time = timeit.default_timer()

    execution_time = end_time - start_time
    print(f"Шаблон: '{pattern}', Таблица: '{table_name}', Время выполнения: {execution_time:.10f} секунд")

def test_select(test_db):
    patterns = ["A%", "_A%", "B%"]
    tables = ["users", "users_ind"]
    print()

    for pattern in patterns:
        for table in tables:
            query = text(f"SELECT * FROM {table} WHERE name LIKE '{pattern}'")
            run_query(test_db, query, pattern, table)

if __name__ == "__main__":
    print("Запуск тестов...")
    pytest.main()
