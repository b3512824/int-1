import pytest
from sqlalchemy import create_engine, text
from dotenv import load_dotenv
import os

# Загрузка переменных окружения
load_dotenv()

# URL для подключения
url = os.getenv("DB_URL")

# Создать объект engine
engine = create_engine(url)

# Проверка подключения
with engine.connect() as connection:
    result = connection.execute(text("SELECT DATABASE();"))
    database_name = result.fetchone()[0]
    print(f"Connected to database: {database_name}")

@pytest.fixture
def test_db():
    with engine.connect() as connection:
        yield connection

def test_select_str_like_pattern(test_db):
    queries = [
        "SELECT * FROM users_ind WHERE name LIKE 'A%'",
        "SELECT * FROM users WHERE name LIKE '%a%'",
        "SELECT * FROM users_ind WHERE name LIKE '%o%'",
        "SELECT * FROM users WHERE name LIKE '_o%'"
    ]

    for query in queries:
        result_with_index = test_db.execute(text(query)).fetchall()
        result_without_index = test_db.execute(text(query)).fetchall()

        if result_with_index == result_without_index:
            print(f"Результаты выполнения запроса '{query}' совпадают.")
        else:
            raise AssertionError(f"Результаты выполнения запроса '{query}' не совпадают.")



if __name__ == "__main__":
    print("Запуск тестов...")
    pytest.main()