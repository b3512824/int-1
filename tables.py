import random
import string
from sqlalchemy import create_engine, Column, String, Integer, Index
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
from dotenv import load_dotenv
import os

load_dotenv()

# URL для подключения
url = os.getenv("DB_URL")

engine = create_engine(url)

Base = declarative_base()

class TableWithIndex(Base):
    __tablename__ = 'test_ind'

    id = Column(Integer, primary_key=True)
    name = Column(String(255), index=True)
    age = Column(Integer)
    address = Column(String(255))

Base.metadata.create_all(engine)

class TableWithoutIndex(Base):
    __tablename__ = 'test'

    id = Column(Integer, primary_key=True)
    name = Column(String(255))
    age = Column(Integer)
    address = Column(String(255))

Base.metadata.create_all(engine)

Session = sessionmaker(bind=engine)
session = Session()

data_to_insert = [{'name': ''.join(random.choices(string.ascii_letters, k=5)),
                   'age': random.randint(18, 80),
                   'address': ''.join(random.choices(string.ascii_letters, k=10))}
                  for _ in range(300000)]

for data in data_to_insert:
    session.add(TableWithIndex(**data))
session.commit()

for data in data_to_insert:
    session.add(TableWithoutIndex(**data))
session.commit()

session.close()
